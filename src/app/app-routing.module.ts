import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '',  redirectTo: 'account/login', pathMatch: 'full' 
  },{
    path:'my-wallet',
    loadChildren:'./components/my-wallet/my-wallet.module#MyWalletModule'
  },{
      path:'requests/id-verification',
      loadChildren:'./components/requests/idverification/idverification.module#IDVerificationModule'
  },{
    path:'requests/property-listing',
    loadChildren:'./components/requests/property-listing/property-listing.module#PropertyListingModule'
  }
  ,{
    path:'requests/withdrawal',
    loadChildren:'./components/requests/withdrawal/withdrawal.module#WithdrawalModule'
  },{
    path:'account',
    loadChildren:'./components/account/account.module#AccountModule'
  },{ path: '**',  redirectTo: '', pathMatch: 'full' } //PAGE NOT FOUND

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
