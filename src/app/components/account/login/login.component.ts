import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router }  from '@angular/router';
import {ValidationsService} from '../../../services/validations/validations.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../../services/user/auth/auth.service';
import {SharedService} from '../../../services/shared/shared.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user;
  public submiting = false;
  public captchaProcessed = true;
  private toastOptions = this.sharedService.toastOptions;
  public recapchaKey = environment.recapchaKey;

  public itemForm = this.fb.group ({
        email: ['',[Validators.required, Validators.email], this.validationService.emailIsNotVerified.bind(this)],
        password: ['',[Validators.required]]
  });

  constructor(private router: Router,
              private authService:AuthService,
              private validationService:ValidationsService,
              public sharedService:SharedService,
              private fb: FormBuilder,
              private toast: ToastService) {


    this.user={};

  }


  ngOnInit() {
  }

  public login(){
		this.submiting=true;
    let req = this.itemForm.value
    this.itemForm.disable();
		this.authService.login(req)
      .then(()=>{
        this.toast.success('Logged in successfully','Welcome', this.toastOptions);
  		}).catch((err)=>{
  			this.submiting=false;
        this.itemForm.enable();
        this.toast.error(err.message,'', this.toastOptions);
  		});
		
  }

  public handleCorrectCaptcha(e){
    this.captchaProcessed = true;
  }

  public emailErrorMessage():string{
    // console.log('errors:',this.itemForm.get('email').errors);

    if (this.itemForm.get('email').hasError('email')) {
      // console.log('not an emal')
      return 'Invalid format';
    }else if(this.itemForm.get('email').hasError('emailNotVerified')){
      return 'Email not verified';
    } else if(this.itemForm.get('email').hasError('required')){
      // console.log('required');
      return 'This field is required';
    } else{
      return ' ';
    }
  }


}



