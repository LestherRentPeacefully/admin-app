/* Angular 2 modules */

import { NgModule } from '@angular/core';

/*Routes*/
import { AccountRoutingModule } from './account-routing.module';

/* Shared modules */

import { SharedModule } from './../../components/shared/shared.module';

/* Components */
import { AccountComponent } from './account.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [AccountComponent, LoginComponent],
  imports: [
    SharedModule,
    AccountRoutingModule
  ]
})
export class AccountModule { }
