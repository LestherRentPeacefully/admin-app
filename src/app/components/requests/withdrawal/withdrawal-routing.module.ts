import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Components */

import { WithdrawalComponent } from './withdrawal.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';


const routes: Routes = [{
	path:'',
	component:WithdrawalComponent,
	children:[{
		path:'all',
		component:AllComponent
	},{
		path:'details/:id',
		component:DetailsComponent
	}]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WithdrawalRoutingModule { }