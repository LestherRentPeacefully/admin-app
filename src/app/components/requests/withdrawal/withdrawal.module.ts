/* Angular 2 modules */

import { NgModule } from '@angular/core';


/*Routes*/

import { WithdrawalRoutingModule } from './withdrawal-routing.module';


/* Shared modules */

import { SharedModule } from './../../../components/shared/shared.module';


/* Components */

import { WithdrawalComponent } from './withdrawal.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [WithdrawalComponent, AllComponent, DetailsComponent],
  imports: [
    SharedModule,
    WithdrawalRoutingModule
  ]
})
export class WithdrawalModule { }
