import { Component, OnInit } from '@angular/core';
import {RequestsService} from '../../../../services/requests/requests.service';
import {SharedService} from '../../../../services/shared/shared.service';
import {DepositWalletService} from '../../../../services/wallet/deposit-wallet/deposit-wallet.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {BigNumber} from 'bignumber.js';


@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  public withdrawals;
  public currencies;
	private toastOptions = this.sharedService.toastOptions;

  constructor(
    private requestsService:RequestsService,
    private sharedService:SharedService,
    private depositWalletService:DepositWalletService,
    private toast: ToastService
  ) { }

  async ngOnInit() {
    [this.withdrawals, this.currencies] = await Promise.all([
      this.requestsService.getWithdrawalRequests(), 
      this.depositWalletService.getCurrencies()
    ]);
  }

  public assetFromCurrency(currency:string){
    return this.currencies.find(asset=>asset.currency==currency);
  }

  public formatDateSring(date:number){
  	let formatedDate = this.sharedService.formatDate(date);
  	return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year}`;
  }

  public formatPreviewTxDate(unFormatedDate:number | Date){
    let date = new Date(unFormatedDate);

    let obj = {
      dayOfMonth : this.sharedService.daysOfTheMonthOptions.find(option=>option.value==String(date.getDate())).value,
      month : this.sharedService.monthsOptions.find(option=>option.value==String(date.getMonth()+1)).label.substr(0,3).toUpperCase()
    }

    return `${obj.month} ${obj.dayOfMonth}`;
  }

  public newBigNumber(value){
    return new BigNumber(value);
  }

  public fromCryptoToUSD(currency:string, amount:string | number | BigNumber){
    return new BigNumber(amount).times(this.assetFromCurrency(currency).price.USD).decimalPlaces(10).toString(10);
  }

}
