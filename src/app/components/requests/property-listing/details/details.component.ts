/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import {RequestsService} from '../../../../services/requests/requests.service';
import {ToastService, IMyOptions} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../../services/shared/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import * as geofirex from 'geofirex';
import * as firebase from 'firebase/app';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  public loading;
  private sub;
  public submiting;
  public listing;
  private toastOptions = this.sharedService.toastOptions;
  public actionSelected;
  public listingID;
  public amenities: Array<any> = this.sharedService.amenities;
  public propertyOptions = this.sharedService.propertyOptions;
  public petsAllowanceOptions = this.sharedService.petsAllowanceOptions;
  public bedsOptions = this.sharedService.bedsOptions;
  public bathsOptions = this.sharedService.bathsOptions;

  public myDatePickerOptions: IMyOptions = {
    minYear: new Date().getFullYear(),
    closeAfterSelect: true
  }

  public itemForm = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    address: ['', [Validators.required,]],
    location: this.fb.group({
      lat: ['', [Validators.required,]],
      lng: ['', [Validators.required,]],
    }),
    unit: ['', []],
    type: ['', [Validators.required,]],
    numOfBeds: ['', [Validators.required,]],
    numOfBaths: ['', [Validators.required,]],
    sqft: ['', [Validators.required, Validators.min(1)]],
    rentAmount: ['', [Validators.required, Validators.min(1)]],
    securityDepositAmount: ['', [Validators.required, Validators.min(0)]],
    details: ['', [Validators.required, Validators.maxLength(1000)]],
    availableFrom: ['', [Validators.required,]],
    // typeOfContract:['', [Validators.required,]],
    // photos: this.fb.array([])
  });

  public rejectionForm = this.fb.group({
  	rejectionReason: ['',[Validators.required]]
  });

  @ViewChild("search") public searchElementRef: ElementRef;


  constructor(private fb: FormBuilder,
              private requestsService:RequestsService,
              private sharedService:SharedService,
              private route: ActivatedRoute,
              private router: Router,
              private toast: ToastService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone) { }

  ngOnInit() {
    this.sub = this.route.paramMap.subscribe(async (params)=>{
      this.listingID = params.get('id');
      this.loading = true;
      try {
        this.listing = await this.requestsService.getListing(this.listingID);

        if(this.listing){
          this.unformatFields();
          this.itemForm.patchValue(this.listing);
          console.log(this.itemForm.value);
        }
      } catch (err) {
        console.error(err.message || err);
	  		this.toast.error('Unexpected error loading property','', this.toastOptions);
      }
      this.loading = false;


      this.mapsAPILoader.load().then(() => {
        let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ["geocode"]
        });
  
        autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
  
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
            if (place.geometry) {
  
              console.log(place);
              this.itemForm.get('address').setErrors(null);
              this.itemForm.get('address').setValue(this.searchElementRef.nativeElement.value);
  
              this.itemForm.get('location').setValue({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
              });
  
  
            } else {
  
              this.itemForm.get('address').setErrors({ wrongAddress: true });
            }
  
          });
        });
      });  



    });

    
  


  }

  ngOnDestroy(){
  	this.sub.unsubscribe();
  }

  public selectAction(action:string){
  	this.actionSelected = action;
  }



  public selectAmenities(amenity: string) {
    let index = this.amenities.findIndex(item => item.name == amenity);
    this.amenities[index].selected = !this.amenities[index].selected;
  }


  private unformatFields(){
    this.listing.type = this.propertyOptions.find(item => item.label == this.listing.type).value;
    this.listing.numOfBeds = this.bedsOptions.find(item => item.label == this.listing.numOfBeds).value;
    this.listing.numOfBaths = this.bathsOptions.find(item => item.label == this.listing.numOfBaths).value;
    let dateTS = new Date(this.listing.availableFrom);
    this.listing.availableFrom = `${dateTS.getFullYear()}-${dateTS.getMonth()+1}-${dateTS.getDate()}`;
    this.listing.photos = this.listing.photos.map((url,index)=>{ return {img:url,thumb:url,description:`Image ${index}`}});
    this.listing.location = {
      lat:this.listing.location.geopoint.latitude,
      lng:this.listing.location.geopoint.longitude
    }
    
    this.amenities = this.amenities.map(amenity=>{
      if(this.listing.amenities[amenity.id]){
        amenity.selected = true;
      }
      return amenity;
    });
    
  }


  private formatFields(fileds: any) {
    let req = fileds;
    req.type = this.propertyOptions.find(item => item.value == req.type).label;
    req.numOfBeds = this.bedsOptions.find(item => item.value == req.numOfBeds).label;
    req.numOfBaths = this.bathsOptions.find(item => item.value == req.numOfBaths).label;

    const geo = geofirex.init(firebase);
    req.location.geohash = geo.point(req.location.lat, req.location.lng).hash;
    let getTimezoneOffset = new Date().getTimezoneOffset()*60*1000; //miliseconds
    req.availableFrom = new Date(req.availableFrom).getTime() + getTimezoneOffset;

    req.amenities = {};
    for (let amenity of this.amenities) {
      if (amenity.selected) req.amenities[amenity.id] = true;
    }

    return req;
  }  

  disableInput(){
  	if (this.actionSelected=='reject') {
  		return !this.rejectionForm.valid;
  	}else if(this.actionSelected=='verify'){
  		return !this.itemForm.valid;
  	}else{
  		return true;
  	}
  }

  async onSubmit(){
    this.submiting = true;
    if (this.actionSelected=='reject') {
      let rejectionReason = this.rejectionForm.get('rejectionReason').value;
      this.rejectionForm.disable();

      try {
        const response = await this.requestsService.rejectListing({
          listingID:this.listingID, 
          rejectionReason
        });

        if (response.success) {
          this.toast.success('Listing request rejected successfully','', this.toastOptions);
          this.router.navigate([`/requests/property-listing/all`]);
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'', this.toastOptions);
        }
        
      } catch (err) {
        console.error(err.message);
        this.toast.error('Unexpected error rejecting listing request','', this.toastOptions);
      }
      this.rejectionForm.enable();



    } else if(this.actionSelected=='verify'){
      let fields = this.formatFields(this.itemForm.value);
      this.itemForm.disable();

      try {
        const response = await this.requestsService.verifyListing(this.listingID, fields, this.listing.user.uid);
        if(response.success){
          this.toast.success('Listing request verified successfully','', this.toastOptions);
          this.router.navigate([`/requests/property-listing/all`]);

        }else{
          console.error(response.error.message);
          this.toast.error(response.error.message,'', this.toastOptions);
        }
      } catch (err) {
        console.error(err.message);
        this.toast.error('Unexpected error verifying listing request','', this.toastOptions);
      }
      this.itemForm.enable();
    }

    this.submiting = false;
  }




}
