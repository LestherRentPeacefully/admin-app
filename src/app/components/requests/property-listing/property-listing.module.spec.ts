import { PropertyListingModule } from './property-listing.module';

describe('PropertyListingModule', () => {
  let propertyListingModule: PropertyListingModule;

  beforeEach(() => {
    propertyListingModule = new PropertyListingModule();
  });

  it('should create an instance', () => {
    expect(propertyListingModule).toBeTruthy();
  });
});
