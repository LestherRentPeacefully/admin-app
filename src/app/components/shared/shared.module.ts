/* Angular modules */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReCaptchaModule } from 'angular2-recaptcha';



/*Bootrap modules  */

import {NavbarModule, 
        WavesModule,
        PreloadersModule,
        InputsModule,
        InputUtilitiesModule,
        ModalModule,
        ChartsModule,
        TabsModule,
        DatepickerModule,
        SelectModule,
        ButtonsModule,
        ChartSimpleModule,
        AccordionModule,
        CollapseModule,
        TooltipModule,
        DropdownModule,
        CardsFreeModule,
        LightBoxModule,
        CharCounterModule,
        CarouselModule,
        BadgeModule,
        IconsModule,
        MaterialChipsModule,
        AutoCompleterModule,
        TableModule  } from 'ng-uikit-pro-standard'; 
 

 /* Components */

import { ScoreComponent } from './score/score.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReCaptchaModule,
    WavesModule,
    InputsModule,
    ModalModule,
    SelectModule,
    ButtonsModule,
    PreloadersModule
  ],
  declarations: [ScoreComponent],
  exports:[
    ScoreComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReCaptchaModule,
    PreloadersModule,
    NavbarModule,
    ChartsModule,
    DatepickerModule,
    SelectModule,
    ChartSimpleModule,
    AccordionModule,  
    LightBoxModule,
    WavesModule,
    InputsModule,
    InputUtilitiesModule,
    ModalModule,
    TabsModule,
    ButtonsModule,
    TooltipModule,
    CollapseModule,
    DropdownModule,
    CardsFreeModule,
    CharCounterModule,
    CarouselModule,
    BadgeModule,
    IconsModule,
    MaterialChipsModule,
    AutoCompleterModule,
    TableModule
  ]
})
export class SharedModule {

}
