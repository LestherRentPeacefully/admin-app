/* Angular 2 modules */

import { NgModule } from '@angular/core';


/*Routes*/

import { MyWalletRoutingModule } from './my-wallet-routing.module';


/* Shared modules */

import { SharedModule } from './../../components/shared/shared.module';

/* Components */

import { IdentityWalletComponent } from './identity-wallet/identity-wallet.component';
import { ReceiveComponent } from './identity-wallet/receive/receive.component';
import { SendComponent } from './identity-wallet/send/send.component';
import { TransactionsComponent } from './identity-wallet/transactions/transactions.component';
import { MyWalletComponent } from './my-wallet.component';

@NgModule({
  declarations: [
    IdentityWalletComponent, 
    ReceiveComponent, 
    SendComponent, 
    TransactionsComponent, 
    MyWalletComponent
  ],
  imports: [
    SharedModule,
    MyWalletRoutingModule
  ]
})
export class MyWalletModule { }
