import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../../../services/user/auth/auth.service';
import { ModalDirective } from 'ng-uikit-pro-standard';
import {EthereumService} from '../../../../services/wallet/ethereum/ethereum.service';
import {BigNumber} from 'bignumber.js';
import { environment } from '../../../../../environments/environment';
import {ValidationsService} from '../../../../services/validations/validations.service';
import {SharedService} from '../../../../services/shared/shared.service'

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.css']
})
export class SendComponent implements OnInit {
  public status = 0;
  public gasPriceInEther;
  public submitting;
  public estimating;
  public txhash;
  private toastOptions = this.sharedService.toastOptions;
  public etherscanURL = environment.etherscanExplorer;
  public minGas:number;

  @Input() identityWallet;
  
  @ViewChild('sendModal') public sendModal:ModalDirective;
  @ViewChild('txModal') public txModal:ModalDirective;

  public itemForm = this.fb.group ({
    to: ['',[ Validators.required, this.validationService.isValidAddress() ]],
    amount: ['',[ Validators.required, Validators.min(new BigNumber(1).times(new BigNumber(10).pow(-18)).toNumber() ) ]],
    gasLimit: ['',[ Validators.required, Validators.min(21000) ]],
    gasPriceInGWei: ['',[ Validators.required, Validators.min(1) ]]
  });

  constructor(private authService:AuthService,
              private toast: ToastService,
              public ethereumService:EthereumService,
              private fb: FormBuilder,
              private validationService:ValidationsService,
              private sharedService:SharedService) {

  }


  ngOnInit() {
  }

  ngOnDestroy(){
  }


  public async next(){
    if (this.itemForm.get('amount').hasError('required') || this.itemForm.get('amount').hasError('min')) { 
      this.toast.error(`Amount must be equal to or greater than 1 wei`,'', this.toastOptions);
    } else if(this.itemForm.get('to').hasError('required') || this.itemForm.get('to').hasError('invalidAddress')){
      this.toast.error(`Invalid address`,'', this.toastOptions);
    } else {

      let req = this.itemForm.value;

      this.estimating = true;
      this.itemForm.get('to').disable();
      this.itemForm.get('amount').disable();

      try {
        let response = await this.ethereumService.estimateGasAndGasPrice({
          from: this.identityWallet.address,
          to: req.to,
          amount:new BigNumber(req.amount).times(new BigNumber(10).pow(18)).toString(10)
        });

        if (response.success) {
          this.itemForm.get('gasPriceInGWei').setValue(new BigNumber(response.data.gasPrice).times(new BigNumber(10).pow(-9)).toNumber());
          this.itemForm.get('gasLimit').setValue(new BigNumber(response.data.estimateGas).toNumber()); // new BigNumber(response.data.estimateGas).times(1.20).integerValue().toNumber();
          this.minGas = new BigNumber(response.data.estimateGas).toNumber();
          this.status=1;

        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'', this.toastOptions);
        }

      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }

      this.estimating = false;
      this.itemForm.get('to').enable();
      this.itemForm.get('amount').enable();

    }
	}


  public newBigNumber(number){
    return new BigNumber(number);
  }


  public async sendTransaction(){
    if (this.itemForm.get('gasLimit').hasError('required') || this.itemForm.get('gasLimit').value<this.minGas) {
      this.toast.error(`Gas limit must be greater or equal than ${this.minGas}`);
    } else if(this.itemForm.get('gasPriceInGWei').hasError('required') || this.itemForm.get('gasPriceInGWei').hasError('min')){
      this.toast.error('Gas Price be greater than 1 GWei','', this.toastOptions);
    }else {

      let req = this.itemForm.value;

      this.submitting = true;
      this.itemForm.get('gasLimit').disable();
      this.itemForm.get('gasPriceInGWei').disable();
      
      try {
        let response = await this.ethereumService.send({
          to:req.to,
          amount:new BigNumber(req.amount).times(new BigNumber(10).pow(18)).toString(10),
          gasPrice: new BigNumber(req.gasPriceInGWei).times(new BigNumber(10).pow(9)).toString(10),
          gasLimit: new BigNumber(req.gasLimit).toString(10)
        });

        if (response.success) {
          this.txhash = response.data;
          this.sendModal.hide();
          this.txModal.show();
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'', this.toastOptions);
        }
        
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }

      this.submitting = false;
      this.itemForm.get('gasLimit').enable();
      this.itemForm.get('gasPriceInGWei').enable();

    }
  }

	public onHidden(){
    this.itemForm.reset();
    this.estimating = false;
    this.submitting = false;
    this.status = 0;
  }


}
