import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentityWalletComponent } from './identity-wallet.component';

describe('IdentityWalletComponent', () => {
  let component: IdentityWalletComponent;
  let fixture: ComponentFixture<IdentityWalletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentityWalletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
