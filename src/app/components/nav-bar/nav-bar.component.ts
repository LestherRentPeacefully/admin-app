import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/user/auth/auth.service';
import {combineLatest} from 'rxjs';
import {SharedService} from '../../services/shared/shared.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
	public userInfo:any;
  public authenticated;
  public app = environment.app;
  public _roleOptions = this.sharedService.roleOptions;
  public roleOptions = this.sharedService.roleOptions;
  public activeRoleString:string;

  constructor(private authService:AuthService,
              public sharedService:SharedService) { }

  ngOnInit() {


    combineLatest(this.authService.authenticated, this.authService.userInfo)
      .subscribe(([authenticated,userInfo])=>{
        this.authenticated = authenticated? authenticated:false;
        this.userInfo = userInfo;
      });

  }

 	public logout(){
  	this.authService.logout();
  }


}
