import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import {SharedService} from '../shared/shared.service';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  private APIURL = environment.APIURL;

  public birthYearOptions=[];

  public expirationYearOptions=[];  


  public IDTypeOptions = [
    {
      value:'1',
      label:'Passport'
    },
    {
      value:'2',
      label:'Drivers Licence'
    },
    {
      value:'3',
      label:'National ID Card'
    }
  ];


  constructor(private sharedService:SharedService,
               public afAuth: AngularFireAuth,
              private http: HttpClient) {

    let lastYear = new Date().getFullYear()-3;
    for(let i = lastYear-100; i<lastYear;i++){
     this.birthYearOptions.push({ value: String(i), label: String(i) });
    }

    let currentYear = new Date().getFullYear();
    for(let i = currentYear; i<currentYear+100;i++){
     this.expirationYearOptions.push({ value: String(i), label: String(i) });
    }


  }

  async getIdVerificationRequests(){
    let docs = [];
    const db = firebase.firestore();
    let snapshots = await db.collection('admin/requests/verifications').get();
    for(let snapshot of snapshots.docs){
      let data = snapshot.data();
      data.id = snapshot.id;
      docs.push(data);
    }
    return docs;
  }


  async getIdVerificationRequest(id:string){
    const db = firebase.firestore();
    let snapshot = await db.doc(`admin/requests/verifications/${id}`).get();
    return snapshot.data();
  }


  async rejectIDRequest(req:{rejectionReason:string, requestID:string, role:string, uid:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/admin/rejectIDRequest`,req,_httpOptions).toPromise();
  }


  async verifyIDRequest(fields:any, id:string,role:string, uid:string):Promise<any>{
    fields.country = this.sharedService.countriesOptions.find((element)=>element.value==fields.country).label.toLowerCase();
    fields.IDType = this.IDTypeOptions.find((element)=>element.value==fields.IDType).label.toLowerCase();

    let req:any ={};
    req.fields = fields;
    req.id = id;
    req.role = role;
    req.uid = uid;

    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/admin/verifyIDRequest`,req,_httpOptions).toPromise();
  }


  async getListings(){
    let docs = [];
    const db = firebase.firestore();
    let snapshots = await db.collection('listings').where('status', '==', 'pending').get();
    for(let snapshot of snapshots.docs){
      let data = snapshot.data();
      data.id = snapshot.id;
      docs.push(data);
    }
    return docs;
  }


  async getListing(listingID:string){
    const db = firebase.firestore();
    let snapshot = await  db.doc(`listings/${listingID}`).get();
    return snapshot.data();
  }


  async rejectListing(req:{listingID:string, rejectionReason:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/admin/rejectListing`,req,_httpOptions).toPromise();
  }


  async verifyListing(listingID:string, fields:any,uid:string):Promise<any>{
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/admin/verifyListing`,{fields, listingID, uid} ,_httpOptions).toPromise();
  }


  async getWithdrawalRequests(){
    let docs = [];
    const db = firebase.firestore();
    let snapshots = await db.collection('transactions').where('type', '==', 'withdrawal').where('status', '==','pending').get();
    for(let snapshot of snapshots.docs){
      let data = snapshot.data();
      data.id = snapshot.id;
      docs.push(data);
    }
    return docs;
  }

  async getWithdrawalRequest(withdrawalID:string){
    const db = firebase.firestore();
    let snapshot = await  db.doc(`transactions/${withdrawalID}`).get();
    return snapshot.data();
  }

  async rejectWithdrawalRequest(req:{withdrawalID:string, rejectionReason:string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/admin/rejectWithdrawalRequest`, req, _httpOptions).toPromise();
  }

  async processWithdrawalRequest(req:{withdrawalID:string, txHash:string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/admin/processWithdrawalRequest`, req, _httpOptions).toPromise();
  }




}
