import { TestBed } from '@angular/core/testing';

import { DepositWalletService } from './deposit-wallet.service';

describe('DepositWalletService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DepositWalletService = TestBed.get(DepositWalletService);
    expect(service).toBeTruthy();
  });
});
