import { Injectable } from '@angular/core';
import { HttpClient }from '@angular/common/http';
import {SharedService} from '../../shared/shared.service';
import { environment } from '../../../../environments/environment';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DepositWalletService {
  private APIURL = environment.APIURL;

  constructor(
    private http: HttpClient,
    private af: AngularFirestore,
    private sharedService:SharedService) { }

    public async getCurrencies(){
      let currencies = [];
      const db = firebase.firestore();
      let snapshots = await db.collection('currencies').orderBy('name','asc').get();
      for(let snapshot of snapshots.docs){
        currencies.push(snapshot.data());
      }
      return currencies;
    }

    public async getCurrency(currency:string){
      const db = firebase.firestore();
      let data = (await db.doc(`currencies/${currency}`).get()).data();
      return data;
    }

    public async getWithdrawalFee(req:{currency:string}){
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post<any>(`${this.APIURL}/wallet/getWithdrawalFee`, req, _httpOptions).toPromise();
    }
}
