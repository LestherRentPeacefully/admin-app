import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import {BigNumber} from 'bignumber.js';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

declare var require:any;

@Injectable({
  providedIn: 'root'
})
export class SharedService {

	public countriesObj = require('../../../assets/countries.json');

  private APIURL = environment.APIURL;

	public countriesOptions=[];

	public toastOptions = { positionClass:'md-toast-bottom-right', progressBar:true, toastClass:'opacity' };

  public propertyOptions = [
      { value: '1', label: 'House' },
      { value: '2', label: 'Apartment' },
      { value: '3', label: 'Basement' },
      { value: '4', label: 'Condo' },
      { value: '5', label: 'Duplex' },
      { value: '6', label: 'Mobile Home' },
      { value: '7', label: 'Room' },
      { value: '8', label: 'Townhouse' },
      // { value: '100', label: 'Other' },
  ];

  public petsAllowanceOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Yes' },
    { value: '3', label: 'No' },
    { value: '4', label: "Only with Landlord's written consent" },
  ];

  public bedsOptions = [
    { value: '0', label: 'Studio' },
    { value: '1', label: '1' },
    { value: '2', label: '2' },
    { value: '3', label: '3' },
    { value: '4', label: '4' },
    { value: '5', label: '5' },
    { value: '6', label: '6' },
    { value: '7', label: '7' },
    { value: '8', label: '8' },
    { value: '9', label: '9+' },
  ];

  public bathsOptions = [
    // { value: '0', label: '0.5' },
    { value: '1', label: '1' },
    // { value: '2', label: '1.5' },
    { value: '2', label: '2' },
    // { value: '4', label: '2.5' },
    { value: '3', label: '3' },
    // { value: '6', label: '3.5' },
    { value: '4', label: '4' },
    // { value: '8', label: '4.5' },
    { value: '5', label: '5' },
    // { value: '10', label: '5.5' },
    { value: '6', label: '6' },
    // { value: '12', label: '6.5' },
    { value: '7', label: '7' },
    // { value: '14', label: '7.5' },
    { value: '8', label: '8' },
    // { value: '16', label: '8.5' },
    { value: '9', label: '9+' },
  ];

  public roleOptions = [
      { value: '1', label: 'I am a Tenant', role:'tenant'},
      { value: '2', label: 'I am a Landlord', role:'landlord'},
      { value: '3', label: 'I am a Service Provider', role:'service provider'},
      { value: '4', label: 'I am a Real Estate Agent', role:'real estate agent'},
      // { value: '5', label: 'I am a Broker', role:'broker'},
  ];

  public amenities = [
    {
      name:'Washer/Dryer In Unit',
      id:'washerUnit',
      // selected:false
    },{
      name:'Pool',
      id:'pool',
      // selected:false
    },{
      name:'Gym',
      id:'gym',
      // selected:false
    },{
      name:'Elevator',
      id:'elevator',
      // selected:false
    },{
      name:'Parking Spot',
      id:'parking',
      // selected:false
    },{
      name:'Fireplace',
      id:'fireplace',
      // selected:false
    },{
      name:'Air Conditioning',
      id:'airConditioning',
      // selected:false
    },{
      name:'Doorman',
      id:'doorman',
      // selected:false
    },{
      name:'Dishwasher',
      id:'dishwasher',
      // selected:false
    },{
      name:'Deck',
      id:'roofDeck',
      // selected:false
    },{
      name:'Storage',
      id:'storage',
      // selected:false
    },{
      name:'Wheelchair Accessible',
      id:'wheelchairAccessible',
      // selected:false
    },{
      name:'Hardwood Floors',
      id:'hardwoodFloors',
      // selected:false
    },{
      name:'Garden',
      id:'garden',
      // selected:false
    },{
      name:'Balcony',
      id:'balcony',
      // selected:false
    },{
      name:'Furnished',
      id:'furnished',
      // selected:false
    },{
      name:'View',
      id:'view',
      // selected:false
    } ,{
      name:'Student Friendly',
      id:'studentFriendly',
      // selected:false
    },{
      name:'High Rise',
      id:'highRise',
      // selected:false
    },{
      name:'Utilities Included',
      id:'utilitiesIncluded',
      // selected:false
    }
  ];



  public serviceProviderSkills = [
    {
      name:'General Labor',
      id:'generalLabor'
    },
    {
      name:'Computer Support',
      id:'computerSupport'
    },
    {
      name:'Handyman',
      id:'handyman'
    },
    {
      name:'Painting',
      id:'painting'
    },
    {
      name:'Drafting',
      id:'drafting'
    },
    {
      name:'Housework',
      id:'housework'
    },
    {
      name:'Electric Repair',
      id:'electricRepair'
    },
    {
      name:'Shopping',
      id:'shopping'
    },
    {
      name:'Appliance Repair',
      id:'applianceRepair'
    },
    {
      name:'Carpentry',
      id:'carpentry'
    },
    {
      name:'Cooking And Recipes',
      id:'cookingAndRecipes'
    },
    {
      name:'Extension And Additions',
      id:'extensionAndAdditions'
    },
    {
      name:'Plumbing',
      id:'plumbing'
    },
    {
      name:'Sewing',
      id:'sewing'
    },
    {
      name:'Appliance Installation',
      id:'applianceInstallation'
    },
    {
      name:'Building',
      id:'building'
    },
    {
      name:'Drone Photography',
      id:'dronePhotography'
    },
    {
      name:'Event Staffing',
      id:'eventStaffing'
    },
    {
      name:'Home Automation',
      id:'homeAutomation'
    },
    {
      name:'Interiors',
      id:'interiors'
    },
    {
      name:'Make up',
      id:'makeUp'
    },
    {
      name:'Cooking / Backing',
      id:'cooking_backing'
    },
    {
      name:'Furniture Assembly',
      id:'furnitureAssembly'
    },
    {
      name:'House Cleaning',
      id:'houseCleaning'
    },
    {
      name:'Landscape Design',
      id:'landscapeDesign'
    },
    {
      name:'Mural Painting',
      id:'muralPainting'
    },
    {
      name:'Commercial Cleaning',
      id:'commercialCleaning'
    },
    {
      name:'Domestic Cleaning',
      id:'domesticCleaning'
    },
    {
      name:'Air Conditioning',
      id:'airConditioning'
    },
    {
      name:'Bathroom',
      id:'bathroom'
    },
    {
      name:'Building Certification',
      id:'buildingCertification'
    },
    {
      name:'Carpet Cleaning',
      id:'carpetCleaning'
    },
    {
      name:'Carpet Repair And Laying',
      id:'carpetRepairAndLaying'
    },
    {
      name:'Decking',
      id:'decking'
    },
    {
      name:'Decoration',
      id:'decoration'
    },
    {
      name:'Embroidery',
      id:'embroidery'
    },
    {
      name:'Gardening',
      id:'gardening'
    },
    {
      name:'Home Organization',
      id:'homeOrganization'
    },
    {
      name:'Installation',
      id:'installation'
    },
    {
      name:'Hot Water Installation',
      id:'hotWaterInstallation'
    },
    {
      name:'Kitchen',
      id:'kitchen'
    },
    {
      name:'Landscape and Gardening',
      id:'landscapeAndGardening'
    },
    {
      name:'Lighting',
      id:'lighting'
    },
    {
      name:'Piping',
      id:'piping'
    },
    {
      name:'Antenna Services',
      id:'antennaServices'
    },
    {
      name:'Asphalt',
      id:'asphalt'
    },
    {
      name:'Bracket Installation',
      id:'bracketInstallation'
    },
    {
      name:'Car Washing',
      id:'carWashing'
    },
    {
      name:'Gas Fitting',
      id:'gasFitting'
    },
    {
      name:'Gutter Installation',
      id:'gutterInstallation'
    },
    {
      name:'Heating Systems',
      id:'heatingSystems'
    },
    {
      name:'Pet Sitting',
      id:'petSitting'
    },
    {
      name:'Pavement',
      id:'pavement'
    },
    {
      name:'Tiling',
      id:'tiling'
    },
    {
      name:'Lawn Mowing',
      id:'lawnMowing'
    },
    {
      name:'Laundry and Ironing',
      id:'laundryAndIroning'
    },
    {
      name:'Locksmith',
      id:'locksmith'
    },
    {
      name:'Bricklaying',
      id:'bricklaying'
    },
  ];

  
  public daysOfTheWeekOptions = [
      { value: '0', label: 'Sunday' },
      { value: '1', label: 'Monday' },
      { value: '2', label: 'Tuesday' },
      { value: '3', label: 'Wednesday' },
      { value: '4', label: 'Thurday' },
      { value: '5', label: 'Friday' },
      { value: '6', label: 'Saturday' },
  ];


  public daysOfTheMonthOptions = [
      { value: '1', label: '1st' },
      { value: '2', label: '2nd' },
      { value: '3', label: '3rd' },
      { value: '4', label: '4th' },
      { value: '5', label: '5th' },
      { value: '6', label: '6th' },
      { value: '7', label: '7th' },
      { value: '8', label: '8th' },
      { value: '9', label: '9th' },
      { value: '10', label: '10th' },
      { value: '11', label: '11st' },
      { value: '12', label: '12th' },
      { value: '13', label: '13rd' },
      { value: '14', label: '14th' },
      { value: '15', label: '15th' },
      { value: '16', label: '16th' },
      { value: '17', label: '17th' },
      { value: '18', label: '18th' },
      { value: '19', label: '19th' },
      { value: '20', label: '20th' },
      { value: '21', label: '21st' },
      { value: '22', label: '22nd' },
      { value: '23', label: '23rd' },
      { value: '24', label: '24th' },
      { value: '25', label: '25th' },
      { value: '26', label: '26th' },
      { value: '27', label: '27th' },
      { value: '28', label: '28th' },
      { value: '29', label: '29th' },
      { value: '30', label: '30th' },
      { value: '31', label: '31st' }
  ];

  public monthsOptions = [
      { value: '1', label: 'January' },
      { value: '2', label: 'February' },
      { value: '3', label: 'March' },
      { value: '4', label: 'May' },
      { value: '5', label: 'April' },
      { value: '6', label: 'June' },
      { value: '7', label: 'July' },
      { value: '8', label: 'August' },
      { value: '9', label: 'September' },
      { value: '10', label: 'October' },
      { value: '11', label: 'November' },
      { value: '12', label: 'December' }
  ];




  constructor(private http: HttpClient,
              public afAuth: AngularFireAuth,
              private storage: AngularFireStorage) {

    for(let i in this.countriesObj){
      this.countriesOptions.push({ value: String(i), label: this.countriesObj[i].name.common });
    }

  }

  public formatDate(unFormatedDate:number | Date){
      let date = new Date(unFormatedDate);
      let obj = {
        dayOfTheWeek : this.daysOfTheWeekOptions.find(option=>option.value==String(date.getDay())).label,
        dayOfMonth : this.daysOfTheMonthOptions.find(option=>option.value==String(date.getDate())).label,
        month : this.monthsOptions.find(option=>option.value==String(date.getMonth()+1)).label,
        year : date.getFullYear(),
        hours : date.getHours(),
        minutes : date.getMinutes(),
        seconds : date.getSeconds()
      
      };
      return obj;  
  }

  public contactSupport(user:{name:string, subject:string,email:string,message:string}):Promise<any>{
    return this.http.post(`${this.APIURL}/support/contact`,user,httpOptions).toPromise();
  }


  private getFileExtension(type: any): string{
      switch (type) {
          case 'image/jpeg':
              return 'jpeg';
          case 'image/jpg':
              return 'jpg';
          case 'image/gif':
              return 'gif';
          case 'image/png':
              return 'png';
          case 'image/svg+xml':
              return 'svg';
          default:
              return '';
      }
  }

  public newFileName(type:string):string{
    const db = firebase.firestore();
    let id = db.collection('uniqueID').doc().id;
    return `${id}.${this.getFileExtension(type)}`;
  }

  public deletePhoto(filePath:string){ 
    let fileRef =this.storage.storage.ref(filePath);
    return fileRef.delete();
  }

  public photoPathFromURL(url:string){
    return this.storage.storage.refFromURL(url).fullPath;
  }

  async getUser(uid:string){
    const db = firebase.firestore();
    let snapshot = await db.doc(`users/${uid}`).get();
    return snapshot.data();
  }

  getToken(){
    return this.afAuth.auth.currentUser.getIdToken(true);
  }

  async getAuthHeader(){
    let token = await this.getToken();
    return { headers : httpOptions.headers.append('Authorization', token) };
  }

  public formatBalance(value, decimals){
    return new BigNumber(value).times(new BigNumber(10).pow(-decimals)).decimalPlaces(10).toString(10); 
  }

  public async getCurrencies(){
    let currencies = [];
    const db = firebase.firestore();
    let snashots = await db.collection('currencies').orderBy('name','asc').get();
    for(let snashot of snashots.docs){
      currencies.push(snashot.data());
    }
    return currencies;
  }


}
