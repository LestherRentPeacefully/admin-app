import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { map} from 'rxjs/operators';
import {AuthService} from '../../services/user/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class GuardsService {

  constructor(private router: Router,private authService: AuthService) { }

  canActivate() {
  	return this.checkLogin();
  }

  canLoad(){
    return this.checkLogin();
  }

  checkLogin(){
    return this.authService.authenticated.pipe(map(
        authenticated=>{
        if (authenticated) { 
           return true;
        } else {
           this.router.navigate(['/account/login']);
           return false;
        }
      }
      ));
  }


}
