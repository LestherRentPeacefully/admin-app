import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import {BehaviorSubject, Observable, Subscription, combineLatest} from 'rxjs';
import { Router }  from '@angular/router';
import { environment } from '../../../../environments/environment';
import {ToastService} from 'ng-uikit-pro-standard';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import {SharedService} from '../../shared/shared.service';
import { filter } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

	private APIURL = environment.APIURL;
	public userInfo = new BehaviorSubject(undefined);
	public authenticated = new BehaviorSubject(undefined);
	public userSession:Subscription;
  public logginSession:Subscription;
  public userSnapshot:any;
	private toastOptions = this.sharedService.toastOptions;
  

  constructor(private http: HttpClient,
  						private router: Router,
  						public afAuth: AngularFireAuth,
              public afs: AngularFirestore,
              public sharedService:SharedService,
  						private toast: ToastService) {


  	this.logginSession = this.stateChanged().subscribe((user)=>{
  		if (user) {
  			console.log("logged in");
  			this.router.navigate(['/my-wallet']); 
  			this.authenticated.next(true);

  			this.userSession = this.getUser(user.uid).pipe(filter((a)=> !!a[0] && !!a[1] && !!a[2]))
          .subscribe((snapshot)=>{
            
            this.userInfo.next({
              ...snapshot[0],
              role: (<any>snapshot[1]).role, 
              balances: this.formatBalances(snapshot[2])
            });
             

    			}, (err)=>{
            console.log(err.message || err);
            console.error("An unexpected error occured, log in again", err);
            this.logout();
          });
  		} else {     
        console.log("Logged out"); 
  			this.authenticated.next(false);
  			this.userInfo.next(false);
  		}
  	});

  }



  public login(user:{email:string,password:string}){
    return new Promise((resolve,reject)=>{    
      this.afAuth.auth.setPersistence('none')
        .then(()=>{
          this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
          .then(()=>{
            resolve(true);
          }).catch((err)=>{
            reject(err);
          });
        }).catch((err)=>{
          this.toast.error('','There was an error. Please, log in again', this.toastOptions);
          reject(err);
        });
    });
  }

  public logout():void{
  	this.afAuth.auth.signOut().then(()=>{
      this.userSession.unsubscribe();
      this.toast.success('Logged out successfully','', this.toastOptions);
      this.router.navigate(['/']);
    });
  }

  public stateChanged(): Observable<any>{
  	return new Observable(observer=>{
    	this.afAuth.auth.onAuthStateChanged((user) =>{
  			observer.next(user);
  		});
   	});
   }

  private getUser(uid:string){
    return combineLatest(
      this.afs.doc(`users/${uid}`).valueChanges(),
      this.afs.doc(`users/${uid}/settings/preferences`).valueChanges(),
      this.afs.collection(`balances`, ref=>ref.where('uid','==',uid)).valueChanges());
  }

  private formatBalances(balances:any[]){
    let balanceObj = {};
    for(let balance of balances){
      balanceObj[balance.currency] = balance;
    }
    return balanceObj;
  }


}
