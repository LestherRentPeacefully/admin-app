import { Component, OnInit  } from '@angular/core';
import { Router, NavigationStart, NavigationCancel, NavigationEnd ,NavigationError } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public loading;

  constructor(private router: Router){
    this.loading = true;
  }


  ngOnInit() {

    document.querySelector('body').style.backgroundColor = 'white';
	}


  ngAfterViewInit() {
   this.router.events.subscribe((event) => {
        if(event instanceof NavigationStart) {
            this.loading = true;
        }
        else if (
            event instanceof NavigationEnd || 
            event instanceof NavigationCancel ||
            event instanceof NavigationError
            ) {
            this.loading = false;
        }
    });
  }



}
